# Pac-u-lator<sup>&dagger;</sup> - A Paca Emulator

> _It's a [**psycho-pac-u-lator**]. It creates a cloud of
> radically-fluctuating free-deviant chaotrons which penetrate
> the synaptic relays. It's concatenated with a synchronous
> transport switch that creates a virtual tributary. It's
> focused onto a biobolic reflector and what happens is that
> hallucinations become reality and the brain is literally
> fried from within._
> ...
> _They used to say it couldn't be built. The equations were
> so complex that most of the scientists that worked on it
> wound up in the insane asylum._  
> -- Dr. Heller ["Mystery Men" (1999)](https://en.wikipedia.org/wiki/Mystery_Men)

> _...I mean, is it a perfect [**package**]? ... No! ...
> And I think that's what I like about it.  It's..._  
> -- Captain Amazing ["Mystery Men" (1999)](https://en.wikipedia.org/wiki/Mystery_Men)

----

![Paculator Virtual Control Surface (VCS) example](paculator-vcs.png "Paculator Virtual Control Surface (VCS)")

## BACKROUND

The
[Pacarana](http://kyma.symbolicsound.com/kyma-sound-design-environment/)
and its baby sister, the Paca, are the hardware side of a sound design
workstation built by [Symbolic Sound
Corporation](http://kyma.symbolicsound.com/about-symbolic-sound/). The
software side is Kyma 7.

The software that I am developing for the Kyma Sound Design
Environment, as Symbolic Sound refers to it, needs to communicate with
the Pacarana via Kyma. Unfortunately, I have rather limited access to
the equipment.

Fortunately, said equipment understands the [Open Sound Control
(OSC)](https://en.wikipedia.org/wiki/Open_Sound_Control) protocol, and
with the proper nudging, can tell me a lot about its current
configuration via [JSON](https://en.wikipedia.org/wiki/JSON) output.

Thus, a rudimentary emulator was born.

## ACKNOWLEDGMENTS

**MANY** thanks to Carla Scaletti and Kurt J. Hebel of the brains,
hearts and souls of [Symbolic Sound
Corporation](https://en.wikipedia.org/wiki/Symbolic_Sound_Corporation),
who put up with my endless badgering and confusion, as well as several
folks on Symbolic Sound's [Kyma Q&A
forum](http://kyma.symbolicsound.com/qa/).

Thanks also to [Guido van
Rossum](https://en.wikipedia.org/wiki/Guido_van_Rossum) et al for
[Python](https://en.wikipedia.org/wiki/Python_(programming_language)),
the Qt folks for [Qt5](http://doc.qt.io/qt-5/),
["attwad"](https://github.com/attwad) for
[python-osc](https://github.com/attwad/python-osc) and [Marek
Wiewiorski](https://github.com/mwicat) for his spin on it and
billions of people at [Stack
Overflow](https://en.wikipedia.org/wiki/Stack_Overflow).

A toast to the goode Mr. Flint and Dave "The Geek" LaFreniere of [The
Barre Open Systems Institute of Vermont](http://www.bosivt.org/) for
being the first victims... er... beta-testers.

And last but not least, a grateful nod to two of the original
trouble-makers, [Richard M. Stallman
(RMS)](https://en.wikipedia.org/wiki/Richard_Stallman)
a.k.a.[St. IGNUcius of the Church of
Emacs](https://en.wikipedia.org/wiki/Editor_war#Humor) and [Linus
Torvalds](https://en.wikipedia.org/wiki/Linus_Torvalds).

## CAVEAT

This was originally designed for Linux, where Python 3 is pretty well
established as the standard. However, the Pacarana itself is connected
to a system running Mac OS X, and ["Think
different"](https://en.wikipedia.org/wiki/Think_different) doesn't
mean "Think better". Getting Python 3 to work correctly on the Mac
just led down a never-ending rabbit-hole of chasing through bizarre
directory structures in search of the next broken thing. So, Python 2
on the Mac, it is.

## DEPENDENCIES INSTALLATION

This code is currently being developed on an Ubuntu Linux system. To
install the dependencies, run the `setup.sh` provided:

        ./setup.sh

Or if you prefer, you can execute the commands manually:

        # Install system-wide packages:
        #
        sudo apt-get -qq update
        sudo apt install python3 python3-setuptools python3-wheel python3-pip \
                         python3-dev python3-pyqt5 avahi-utils avahi-discover
        
        sudo -H pip3 install virtualenv virtualenv-clone virtualenvwrapper
        
        # Create a local virtual environment, now that you have the
        # global, system-wide dependencies satisfied, and install the
        # Python packages into that:
        #
        mkdir ~/.virtualenvs/
        virtualenv --python=/usr/bin/python3 ~/.virtualenvs/paculator
        source ~/.virtualenvs/paculator/bin/activate
        pip3 install -r requirements.txt
        
        # Install paculator configuration file and a sample
        # JSON dump to start you off:
        #
        mkdir -p ~/.config
        cp paculator.conf ~/.config/
        mkdir -p ~/.local/share/paca
        cp dumps/kyma.json ~/.local/share/paca/

If you're not on a Linux system, you'll need to figure out how to get
Qt5 installed properly in order for the Python 3 Qt5 bindings to
work. On Mac OS X, the **avahi** packages are replaced by Mac OS X's
native built-in Bonjour technologies.

This code is working with Python 3.5, Qt 5 version 5.6.1, and PyQt5
version 5.9.2.

### On a Raspberry Pi with Pipenv

The new recommendation from the Python Software Foundation is to move
away from `virtualenv` and `pip` to the newer, combined `pipenv`. As
of 2019.01.12, PyQt5 on the Raspberry Pi, running Raspbian was
problematic and had to be installed from source, which takes a very
long time.

First, if a pipenv virtual environment is already set up, and giving
trouble, get rid of it via:

        $ rm -rf ~/.local/share/virtualenvs/paculator-...
        $ rm -rf ~/.cache/pipenv
        $ rm Pipfile.lock 
        $ pipenv lock --clear

Note: The name and location of the paculator... virtual environment in
the first line will vary from system to system. I leave it as an exercise
to the reader to find their own virtual environment.

Next:

        $ pipenv shell
        $ pipenv update

The update failed while fetching PyQt5. While still in the virtual
environment, do the following (about 2.5 hours on the Raspberry Pi).
**STAY IN THE VIRUAL ENVIRONMENT and install `sip` and `PyQt5` from
source.**

Following the ["Installing PyQt5 on raspberry: unable to imoprt
 PyQt5.sip"](https://raspberrypi.stackexchange.com/a/87796/11215)
 answer from the Raspberry Pi StackExchange forum. (See also ["PyQt5
 on a Raspberry
 Pi"](https://raspberrypi.stackexchange.com/a/63058/11215) but note
 the caveat in the comments. Specifically:

* First [install
  `sip`](http://pyqt.sourceforge.net/Docs/sip4/installation.html). As
  of this writing, the source can be found at
  [https://www.riverbankcomputing.com/software/sip/download](https://www.riverbankcomputing.com/software/sip/download).

        $ wget https://sourceforge.net/projects/pyqt/files/sip/sip-4.19.13/sip-4.19.13.tar.gz
        $ tar xzvf sip-4.19.13.tar.gz
        $ cd sip-4.19.13
        $ python3 configure.py --sip-module=PyQt5.sip
        $ make
        $ sudo make install

* Then [install
  `PyQt5`](http://pyqt.sourceforge.net/Docs/PyQt5/installation.html). As
  of this writing, the source can be found at
  [https://www.riverbankcomputing.com/software/pyqt/download5](https://www.riverbankcomputing.com/software/pyqt/download5).

        $ wget https://sourceforge.net/projects/pyqt/files/PyQt5/PyQt-5.11.3/PyQt5_gpl-5.11.3.tar.gz
        $ tar xzvf PyQt5_gpl-5.11.3.tar.gz
        $ cd PyQt5_gpl-5.11.3
        $ python3 configure.py
        $ make
        $ sudo make install

## FUTURE PLANS

This was done for my needs, but there appears to be more interest from
other quarters. So, I will probably add a GUI for file selection, and
possibly, a configuration settings menu for the default directory.

The data sent back from the Paca includes enough information to
recreate the Virtual Control Surface (VCS) that Kyma provides. (See image.)
Work on implementing that is in the very early stages.

![Kyma Virtual Control Surface (VCS)](vcs.png "Kyma Virtual Control Surface (VCS)")

[An example of Kyma's Virtual Control Surface (VCS)]

<sup>&dagger;</sup> Credit and/or blame for the name goes to [Paul
Flint](https://github.com/flintiii). I'm the party [ir]responsible for
connecting it to the movie ["Mystery
Men"](https://en.wikipedia.org/wiki/Mystery_Men).
