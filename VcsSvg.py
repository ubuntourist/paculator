#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#  VCS
#  Written by Kevin Cole <kevin.cole@novawebdevelopment.org> 2017.08.21
#
#  This will build an SVG from a Paca-dumpped JSON file in preparation for
#  mimicing the Kyma Virtual Control Surface (VCS)
#

"""Read a Paca JSON dump file and create an Virtual Control Surface SVG"""

import os.path
import json


def sanitize(vcs):
    """Convert a list of key/value pairs to a dictionary, as God intended"""

    for control in vcs:
        if isinstance(control, dict)                              and \
           "lookOrNil"        in control                          and \
           isinstance(control["lookOrNil"], dict)                 and \
           "database"         in control["lookOrNil"]             and \
           isinstance(control["lookOrNil"]["database"], dict)     and \
           "flatAssociations" in control["lookOrNil"]["database"] and \
           isinstance(control["lookOrNil"]["database"]["flatAssociations"],
                      list):
            lst = control["lookOrNil"]["database"]["flatAssociations"]  # list
            dct = {}                                                    # dict
            for item in range(0, len(lst), 2):
                dct[lst[item]] = lst[item + 1]
            control["lookOrNil"]["database"]["flatAssociations"] = dct
    return vcs


def rgb(pallet):
    """Scale the colors and convert to an RGB hexadecimal string"""

    hex_rgb  = f"""#"""
    hex_rgb += f"""{int(pallet["red"]   * 255):02x}"""
    hex_rgb += f"""{int(pallet["green"] * 255):02x}"""
    hex_rgb += f"""{int(pallet["blue"]  * 255):02x}"""
    return hex_rgb


def main():
    """The main attraction"""

    paca = os.path.expanduser("~/.local/share/paca")  # Virtual paca JSON files

    display_types  = ["Fader", "Select from List", "Toggle"]  # More someday

    title          = "Kyma Virtual Control Surface (VCS)"
    fill_color     = "#e7e7e7"  # Light gray
    fill_opacity   = 1.0        # Completely opaque (was 0.1)
    stroke_color   = "#a7a7a7"  # Dark gray
    stroke_opacity = 1.0        # Completely opaque (was 0.9)
    stroke_width   = 2          # Not too thick

    frame  = f"fill:{fill_color};"
    frame += f"fill-opacity:{fill_opacity};"
    frame += f"stroke:{stroke_color};"
    frame += f"stroke-opacity:{stroke_opacity};"
    frame += f"stroke-width:{stroke_width};"

    body = ""
    horizontal, vertical = 0, 0  # Full width, full height

    source = input("                   Paca JSON dump file: ")
    destin = input("Virtual Control Surface (VCS) SVG file: ")

    source = os.path.splitext(source)[0]    # Eliminate extension if provided
    paca = f"{paca}/{source}.json"

    vcs = json.load(open(paca))
    vcs = sanitize(vcs)

    for control in vcs:
        labels = None
        if isinstance(control, dict):
            if "displayType" in control and control["displayType"] == "Toggle":
                colors = control["lookOrNil"]["database"]["flatAssociations"]

                off_color  = rgb(colors["vcsButtonDownColor"])
                on_color   = rgb(colors["vcsButtonUpColor"])

                fill_color = on_color

                style  = f"fill:{fill_color};"
                style += f"fill-opacity:{fill_opacity};"
                style += f"stroke:{stroke_color};"
                style += f"stroke-opacity:{stroke_opacity};"
                style += f"stroke-width:{stroke_width};"

                labels = control["tickMarksOrNil"]["rawLabelsString"]
            else:
                style = frame
                labels = None

            for property in control:
                if property == "layout":
                    layout = control[property]
                    x1     = layout["leftOffset"]
                    y1     = layout["topOffset"]
                    x2     = layout["rightOffset"]
                    y2     = layout["bottomOffset"]
                    width  = x2 - x1
                    height = y2 - y1

                    rectangle  = f"""<rect """
                    rectangle += f"""x="{x1}" """
                    rectangle += f"""y="{y1}" """
                    rectangle += f"""width="{width}" """
                    rectangle += f"""height="{height}" """
                    rectangle += f"""style="{style}" />"""
                    body += rectangle

                    if labels:
                        label  = f"""<text """
                        label += f"""x="{x1 + 10}" """
                        label += f"""y="{y1 + 10}">"""
                        label += f"""{labels[0]}</text>"""
                        body  += label

                    horizontal = max(horizontal, x2)
                    vertical   = max(vertical,   y2)

    src = """\
    <?xml version="1.0" encoding="UTF-8" standalone="no"?>
    <!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN"
                         "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
    <svg width="{horizontal}" height="{vertical}"
         viewBox="0 0 {horizontal} {vertical}"
         xmlns="http://www.w3.org/2000/svg" version="1.1">
      <title>{title}</title>
    {body}
    </svg>
    """

    svg = open(destin, "w")
    svg.write(src)
    svg.close()


if __name__ == "__main__":
    main()
