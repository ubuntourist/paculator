#!/bin/bash
# Written by Kevin Cole <kevin.cole@novawebdevelopment.com> 2018.01.02 (kjc)
# Last modified by Kevin Cole <kevin.cole@novawebdevelopment.com> 2024.07.22
#
# For Raspberry Pi see:
#
#      StackExchange: PyQt5 on a Raspberry Pi?
#      https://raspberrypi.stackexchange.com/a/63058/11215
#

# Install the Debian packages needed
#
printf "Installing Debian packages:\n\n"
printf "    python3, python3-pip, python3-dev,\n"
printf "    avahi-utils, lsb-release, perl\n"
sudo apt-get -qq update
sudo apt install python3 python3-dev python3-pip \
                 avahi-utils lsb-release perl

# Install pipenv locally
#
printf "Installing Python packages in userland:\n\n"
printf "    pipenv, setuptools, wheel\n"
mkdir -p "`python3 -m site --user-site`"
pip3 install --user pipenv
pip3 install --user setuptools
pip3 install --user wheel

# Prep the virtual environment
#
rm -rf ~/.local/share/virtualenvs/paculator*
rm Pipfile.lock
cp Pipfile.dist Pipfile

# Install Python packages to the local virtual environment, either for
# Ubuntu / LinuxMint / Pop! OS or Raspbian. Pure Debian users should
# be able to figure out what to do. The rest of you... good luck.
#
printf "Creating and populating a virtual environment...\n"
if [[ $(lsb_release -is) == "Ubuntu"    ]] || \
   [[ $(lsb_release -is) == "LinuxMint" ]] || \
   [[ $(lsb_release -is) == "Pop"       ]]; then
    printf "Installing Ubuntu-specific packages.\n"
    pipenv install PyQt5
elif [[ $(lsb_release -is) == "Raspbian" ]]; then
    printf "Installing Raspbian-specific (Raspberry Pi) packages.\n"
    printf "Kludging installation of PyQt5 and sip to:\n\n"
    sudo apt install python3-sip
    pip3 install --user sip
#   wget https://www.riverbankcomputing.com/static/Downloads/sip/4.19.23/sip-4.19.23.tar.gz
#   wget https://www.riverbankcomputing.com/static/Downloads/PyQt5/5.13.2/PyQt5-5.13.2.tar.gz
    virt=$(find ~/.local/share/virtualenvs/ -name "paculator*")
    dest=$(ls -d $virt/lib/python*)
    pushd $dest/site-packages
    cp -rv /usr/lib/python3/dist-packages/sip*   .
    cp -rv /usr/lib/python3/dist-packages/PyQt5* .
else
    printf "I don't know what distribution this is. Good luck to you.\n"
fi
pipenv install setuptools
pipenv install wheel
pipenv install netifaces
pipenv install zeroconf
pipenv install python-osc
pipenv install docopt

# Install a default configuration file
#
printf "Installing the Pac-u-lator configuration file to ~/.config/\n"
mkdir -p ~/.config
cp paculator.conf ~/.config/

# Install a Kyma Sound (TM) JSON dump file
#
printf "Installing a 'starter' Kyma Sound (TM) JSON dump to ~/.local/share/paca/\n"
mkdir -p ~/.local/share/paca
cp dumps/row_your_boat.json ~/.local/share/paca/
ln -s ~/.local/share/paca/row_your_boat.json ~/.local/share/paca/kyma.json

printf "\nI think I'm done...\n\n"

printf "Test with:\n\n"
printf "    ./paculator vcs\n"
