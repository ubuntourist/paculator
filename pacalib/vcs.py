#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  vcs.py
#
#  This will build an SVG from a Paca-dumpped JSON file in preparation for
#  mimicing the Kyma Virtual Control Surface (VCS)
#
#  Copyright 2017 Kevin Cole <kevin.cole@novawebcoop.org> 2017.12.23
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation; either version 3 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the Free
#  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, USA.
#
#  ACKNOWLEDGEMENTS
#
#  Many thanks to @eyllanesc who took a LOT of time with me:
#
#  * https://stackoverflow.com/users/6622587/eyllanesc
#  * https://gist.github.com/eyllanesc/01ef7eb60efc2554bb7bf69ef271b25c
#  * https://stackoverflow.com/questions/47972967/\
#    how-do-i-add-fit-and-position-dynamically-generated-qgroupbox-on-a-screen
#

import sys
import os
import json

from .finder import find_pacas

from PyQt5.QtCore    import *
from PyQt5.QtGui     import *
from PyQt5.QtWidgets import *

__appname__    = "Pac-u-lator"
__module__     = "VCS"
__author__     = "Kevin Cole"
__copyright__  = "Copyright \N{copyright sign} 2017"
__agency__     = "NOVA Web Development, LLC"
__credits__    = ["Kevin Cole"]  # Authors and bug reporters
__license__    = "GPL"
__version__    = "2.2"
__maintainer__ = "Kevin Cole"
__email__      = "kevin.cole@novawebcoop.org"
__status__     = "Prototype"  # "Prototype", "Development" or "Production"

# Colors
#
VCS_OUTER_BG     = "#c0cbd9"  # "Link water"   (steel blue gray)
VCS_INNER_BG     = "#ccd8e7"  # "Hawkes blue"
VCS_UNFOCUSED    = "#d5d5d5"  # "Light grey"
VCS_GREEN_PUSHED = "#12f785"  # "Spring green"
VCS_GREEN_PULLED = "#9cd2b7"  # "Padua"        (hospital puke green)
VCS_RED_PUSHED   = "#de0d00"  # "Orange red"
VCS_RED_PULLED   = "#ce9a9a"  # "Rosy brown"   (pink-ish)

# Widget types
#
BUTTONS = ["Toggle", "Toggle (fill)", "Button", "Button (fill)"]
DIALS   = ["Potentiometer", "Rotary", "Pan Pot"]
RADIOS  = ["Radio Buttons", "Radio Buttons (fill)",
           "Vertical Radio Buttons", "Vertical Radio Buttons (fill)"]
CHECKS  = ["Check Box"]
FADERS  = ["Fader", "Horizontal Fader", "Small Fader"]


def sanitize(dump):
    """Convert a list of key/value pairs to a dictionary, as God intended"""
    for control in dump:
        if isinstance(control, dict)                              and \
           "lookOrNil"        in control                          and \
           isinstance(control["lookOrNil"], dict)                 and \
           "database"         in control["lookOrNil"]             and \
           isinstance(control["lookOrNil"]["database"], dict)     and \
           "flatAssociations" in control["lookOrNil"]["database"] and \
           isinstance(control["lookOrNil"]["database"]["flatAssociations"],
                      list):
            lst =  control["lookOrNil"]["database"]["flatAssociations"]  # list
            dct = {}                                                     # dict
            for k in range(0, len(lst), 2):
                dct[lst[k]] = lst[k + 1]
            control["lookOrNil"]["database"]["flatAssociations"] = dct
    return dump


def rgb(pallet):
    """Scale the colors and convert to an RGB hexadecimal string"""
    hex_rgb  = f"""#"""
    hex_rgb += f"""{int(pallet["red"]   * 255):02x}"""
    hex_rgb += f"""{int(pallet["green"] * 255):02x}"""
    hex_rgb += f"""{int(pallet["blue"]  * 255):02x}"""
    return hex_rgb


def disemvowel(phrase):
    """Shorten phrases by removing vowels"""
    vowels = ["a", "e", "i", "o", "u"]
    words = phrase.split(" ")
    phrs = ""
    for word in words:
        if phrs:
            phrs += " "
        wrd = word[0]
        for letter in word[1:]:
            if letter not in vowels:
                wrd += letter
        phrs += wrd
    return phrs


class VCS(QWidget):
    """Kyma Virtual Control Surface (VCS)"""

    def __init__(self, *args, **kwargs):
        """VCS constructor"""

        super(VCS, self).__init__(**kwargs)
        self.setObjectName("VCS")
        self.setWindowFlags(Qt.FramelessWindowHint)

        self.screen_width  = None
        self.screen_height = None

        self.font = QFont()
        self.font.setWeight(75)
        self.font.setBold(True)

        self.dump    = sanitize(args[0])
        self.control = {}                 # Lacking all self-controls...

        # Known widget display types. More someday.
        #
        DISPLAY_TYPES  = ["Fader",
                          "Select from List",
                          "Toggle"]

        self.boundary = 5

        window_css  = f"QWidget#VCS {{"
        window_css += f"  border: {self.boundary}px solid #7f7f7f;"
        window_css += f"  background-color: #c0cbd9;"
        window_css += f"}}"
        self.setStyleSheet(window_css)
        self.getKymaDimensions()

        boxes = (item for item in self.dump
                 if item["creationClass"] == "VCSSubLayoutAutomaticComponent")

        controls = (item for item in self.dump
                    if item["creationClass"] == "VCSEventSourceComponent")

        for specs in boxes:
            self.make_widget(specs, "box")

        for specs in controls:
            self.make_widget(specs, "control")

    def getKymaDimensions(self):
        """Get Kyma's VCS full window dimensions"""

        kyma_screen = next((item
                            for item in self.dump
                            if item["creationClass"] == "Association"     and
                               item["key"]           == "screenRectangle" and
                               isinstance(item["value"], dict)),
                           None)
        coordinates = kyma_screen["value"]

        self.sizeKymaScreen = QSize(coordinates["rightOffset"] -
                                    coordinates["leftOffset"],
                                    coordinates["bottomOffset"] -
                                    coordinates["topOffset"])

    def make_widget(self, specs, widget):
        """Build VCS widget from JSON specs"""

        box = QGroupBox(self)
        layout = specs["layout"]
        rect = QRect()
        rect.setTop(layout["topOffset"]       + 2)
        rect.setLeft(layout["leftOffset"]     + 2)
        rect.setBottom(layout["bottomOffset"] - 2)
        rect.setRight(layout["rightOffset"]   - 2)
        box.setGeometry(self.getRect(rect))

        if widget == "box":
            box_css = f""
            if "myLabel" in specs:
                box.setTitle(specs["myLabel"])
                box.setObjectName(specs["myLabel"])
                box_css += f"""QGroupBox::title {{"""
                box_css += f"""  padding: 0px 5px;"""
                box_css += f"""  left: 7px;"""
                box_css += f"""  border: 2px solid #7f7f7f;"""
                box_css += f"""  border-radius: 6px;"""
                box_css += f"""  background-color: #efefef;"""
                box_css += f"""  color: #000000;"""
                box_css += f"""  font-weight: bold;"""
                box_css += f"""  subcontrol-origin: margin;"""
                box_css += f"""}}"""
            box_css += f"""QGroupBox {{"""
            box_css += f"""  background-color: #ccd8e7;"""
            box_css += f"""  margin-top: 6px;"""
            if "borderInset" in specs:
                box_css += f"""padding: {specs["borderInset"]}px;"""
            if "borderRadius" in specs:
                box_css += f"""border-radius: {specs["borderRadius"]}px;"""
            if "borderThickness" in specs:
                box_css += f"""border-width: {specs["borderThickness"]}px;"""
            box_css += f"}}"""
        elif widget == "control":
            box_css  = f"""QGroupBox {{"""
            box_css += f"""  border: 0px;"""
            box_css += f"""  background-color: #ccd8e7;"""
            box_css += f"""}}"""
            control = None
            if   specs["displayType"] in FADERS:
                label = QLabel(box)
                label.setFont(self.font)
                label.setText(specs["label"])
                maxw = layout["rightOffset"] - layout["leftOffset"] - 4
                maxw *= (self.screen_width / self.sizeKymaScreen.width())
                labelw = label.fontMetrics().boundingRect(specs["label"]).width() + 12
                if labelw > maxw:
                    label.setText(disemvowel(specs["label"]))
                label_height = label.height()
                spin_box = QSpinBox(box)  # See my Jukebox.py to finish
                spin_box.setRange(specs["minimum"], specs["maximum"])
                spin_box_height = spin_box.height()
                oldx, oldy = spin_box.pos().x(), spin_box.pos().y()
                newpos = QPoint(oldx, oldy + label_height)
                spin_box.move(newpos)
                control = QSlider(box)
                object_id = specs["label"].replace(" ", "")
                control.setObjectName(object_id)
                control.setToolTip(specs["label"])
                tallness = (layout["bottomOffset"] - layout["topOffset"] - 4)
                tallness *= (self.screen_height / self.sizeKymaScreen.height())
                tallness -= label_height
                tallness -= spin_box_height
                oldx, oldy = control.pos().x(), control.pos().y()
                newpos = QPoint(oldx, oldy + label_height + spin_box_height)
                control.move(newpos)
                control_css  = f"QSlider {{"
                control_css += f"  border: 0px;"
                control_css += f"  min-height: {tallness}px"
                control_css += f"}}"
                control_css += f"QSlider::handle {{"
                control_css += f"  background-color: #fe0f00;"
                control_css += f"  border: 1px solid #000000;"
                control_css += f"}}"
                control.setStyleSheet(control_css)
                control.setMinimum(specs["minimum"])
                control.setMaximum(specs["maximum"])
                control.setTickInterval(specs["grid"])
                control.setSingleStep(specs["grid"])
                control.setPageStep(specs["grid"])
                control.setTickPosition(QSlider.TicksAbove)
            elif specs["displayType"] in BUTTONS:
                control = QPushButton(box)
                if "tickMarksOrNil"  in specs and \
                   isinstance(specs["tickMarksOrNil"], dict) and \
                   "rawLabelsString" in specs["tickMarksOrNil"]:
                    labels = specs["tickMarksOrNil"]["rawLabelsString"]
                else:
                    labels = [specs["label"]]
                object_id = specs["label"].replace(" ", "")
                control.setObjectName(object_id)
                control.setText(labels[0])
                control.setToolTip(labels[0])
                try:
                    colors   = specs["lookOrNil"]["database"]["flatAssociations"]
                    off_color = rgb(colors["vcsButtonDownColor"])
                    on_color  = rgb(colors["vcsButtonUpColor"])
                except KeyError:
                    off_color = vcs_red_pulled
                    on_color  = vcs_green_pulled
                control.setAutoDefault(False)
                control.setDefault(False)
                control.setCheckable(True)
                control.setChecked(False)
                control_css  = f"QPushButton#{object_id}:closed {{"
                control_css += f"  background-color: {off_color};"
                control_css += f"  border: 1px outset #000000;"
                control_css += f"  border-radius: 6px;"
                control_css += f"  padding: 6px;"
                control_css += f"}}"
                control_css += f"QPushButton#{object_id}:open {{"
                control_css += f"  background-color: {on_color};"
                control_css += f"  border: 1px inset #000000;"
                control_css += f"  border-radius: 6px;"
                control_css += f"  padding: 6px;"
                control_css += f"}}"
                control.setStyleSheet(control_css)
                maxw = layout["rightOffset"] - layout["leftOffset"] - 4
                maxw *= (self.screen_width / self.sizeKymaScreen.width())
                control.setMaximumWidth(maxw)
                control.setMinimumWidth(maxw)
                labelw = 0
                labelw = control.fontMetrics().boundingRect(labels[0]).width() + 12
                if labelw > maxw:
                    control.setText(disemvowel(labels[0]))
            elif specs["displayType"] in DIALS:
                label = QLabel(box)
                label.setFont(self.font)
                label.setText(specs["label"])
                maxw = layout["rightOffset"] - layout["leftOffset"] - 4
                maxw *= (self.screen_width / self.sizeKymaScreen.width())
                labelw = label.fontMetrics().boundingRect(specs["label"]).width() + 12
                if labelw > maxw:
                    label.setText(disemvowel(specs["label"]))
                label_height = label.height()
                control = QDial(box)
                tallness  = (layout["bottomOffset"] - layout["topOffset"] - 4)
                tallness *= (self.screen_height / self.sizeKymaScreen.height())
                tallness -= label_height
                oldx, oldy = control.pos().x(), control.pos().y()
                control.setGeometry(oldx, oldy, tallness, tallness)
                object_id = specs["label"].replace(" ", "")
                control.setObjectName(object_id)
                control.setToolTip(specs["label"])
                if "tickMarksOrNil"  in specs and \
                   isinstance(specs["tickMarksOrNil"], dict) and \
                   "rawLabelsString" in specs["tickMarksOrNil"]:
                    labels = specs["tickMarksOrNil"]["rawLabelsString"]
                else:
                    labels = [specs["label"]]
                object_id = specs["label"].replace(" ", "")
                control.setObjectName(object_id)
                control.setToolTip(labels[0])
                oldx, oldy = control.pos().x(), control.pos().y()
                newpos = QPoint(oldx, oldy + label_height)
                control.move(newpos)
                maxw = layout["rightOffset"] - layout["leftOffset"] - 4
                maxw *= (self.screen_width / self.sizeKymaScreen.width())
                control.setMaximumWidth(maxw)
                control.setMinimumWidth(maxw)
            elif specs["displayType"] in CHECKS:
                control = QCheckBox(box)
                object_id = specs["label"].replace(" ", "")
                control.setObjectName(object_id)
                control.setToolTip(specs["label"])
                if "tickMarksOrNil"  in specs and \
                   isinstance(specs["tickMarksOrNil"], dict) and \
                   "rawLabelsString" in specs["tickMarksOrNil"]:
                    labels = specs["tickMarksOrNil"]["rawLabelsString"]
                else:
                    labels = [specs["label"]]
                object_id = specs["label"].replace(" ", "")
                control.setObjectName(object_id)
                control.setText(labels[0])
                control.setToolTip(labels[0])
                try:
                    colors   = specs["lookOrNil"]["database"]["flatAssociations"]
                    off_color = rgb(colors["vcsButtonDownColor"])
                    on_color  = rgb(colors["vcsButtonUpColor"])
                except KeyError:
                    off_color = vcs_red_pulled
                    on_color  = vcs_green_pulled
                control_css  = f"QCheckBox#{object_id}:closed {{"
                control_css += f"  background-color: {off_color};"
                control_css += f"  border: 1px outset #000000;"
                control_css += f"  border-radius: 6px;"
                control_css += f"  padding: 6px;"
                control_css += f"}}"
                control_css += f"QCheckBox#{object_id}:open {{"
                control_css += f"  background-color: {on_color};"
                control_css += f"  border: 1px inset #000000;"
                control_css += f"  border-radius: 6px;"
                control_css += f"  padding: 6px;"
                control_css += f"}}"
                control.setStyleSheet(control_css)
                maxw = layout["rightOffset"] - layout["leftOffset"] - 4
                maxw *= (self.screen_width / self.sizeKymaScreen.width())
                control.setMaximumWidth(maxw)
                control.setMinimumWidth(maxw)
                labelw = 0
                labelw = control.fontMetrics().boundingRect(labels[0]).width() + 12
                if labelw > maxw:
                    control.setText(disemvowel(labels[0]))
            elif specs["displayType"] in RADIOS:
                pass
            elif specs["displayType"] == "Select from List":
                label = QLabel(box)
                label.setFont(self.font)
                label.setText(specs["label"])
                maxw = layout["rightOffset"] - layout["leftOffset"] - 4
                maxw *= (self.screen_width / self.sizeKymaScreen.width())
                labelw = label.fontMetrics().boundingRect(specs["label"]).width() + 12
                if labelw > maxw:
                    label.setText(disemvowel(specs["label"]))
                label_height = label.height()
                control = QComboBox(box)
                object_id = specs["label"].replace(" ", "")
                control.setObjectName(object_id)
                control.setToolTip(specs["label"])
                oldx, oldy = control.pos().x(), control.pos().y()
                newpos = QPoint(oldx, oldy + label_height)
                control.move(newpos)
                control_css  = "QComboBox {"
                control_css += "  border: 0px;"
                control_css += "}"
                control.setStyleSheet(control_css)
                choices = specs["tickMarksOrNil"]["rawLabelsString"]
                for index, choice in enumerate(choices):
                    control.addItem(choice)
            if control:
                self.control[specs["concreteEventID"]] = control
        box.setStyleSheet(box_css)

    def getRect(self, rect):
        """Return VCS rectangle scaled to screen size"""

        screen_resolution = qApp.desktop().screenGeometry()
        self.screen_width  = screen_resolution.width()  - ((self.boundary * 2) + 12)
        self.screen_height = screen_resolution.height() - ((self.boundary * 2) + 12)

        transform = QTransform()
        transform.translate((self.boundary + 6),
                            (self.boundary + 6))
        transform.scale(self.screen_width  / self.sizeKymaScreen.width(),
                        self.screen_height / self.sizeKymaScreen.height())
        return transform.mapRect(rect)

    def keyPressEvent(self, event):
        """Escape upon ESC keypress"""

        press = event.key()
        if press == Qt.Key_Escape:            # "ESCape" hatch emergency exit
            QApplication.closeAllWindows()


def vcs(dump="kyma.json", broadcast="4095"):
    """Start a Pac-u-lator GUI (Virtual Control Surface)"""

    pacas = find_pacas()
    for paca in pacas:
        if paca[0].find(broadcast) > -1:
            print(f"ERROR: beslime-{broadcast} already in use")
            sys.exit(1)

    paca = os.path.expanduser("~/.local/share/paca")  # Virtual paca JSON files

    dump = os.path.splitext(dump)[0]    # Eliminate extension if provided
    paca = f"{paca}/{dump}.json"

    dump = json.load(open(paca))

    QCoreApplication.setApplicationName(__appname__)
    QCoreApplication.setApplicationVersion(__version__)
    QCoreApplication.setOrganizationName("NOVA Web Development, LLC")
    QCoreApplication.setOrganizationDomain("novawebdevelopment.com")

    # Instantiate a vcs, show it and start the app.
    app = QApplication(sys.argv)

    gui = VCS(dump)
    gui.showFullScreen()

    app.exec_()


def main():
    """The main attraction (for stand-alone testing)"""

    _ = os.system("clear")
    print(f"{__appname__} v.{__version__}")
    print(f"{__copyright__} ({__license__})")
    print(f"{__author__}, {__agency__} <{__email__}>")
    print()

    source = input("Paca JSON dump file: ")
    vcs(dump=source)


if __name__ == '__main__':
    main()
