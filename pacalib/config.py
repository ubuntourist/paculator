#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  config.py
#
#  Copyright 2017 Kevin Cole <kevin.cole@novawebcoop.org> 2017.11.28
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation; either version 3 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the Free
#  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, USA.

"""Handle configure command"""

import os
import configparser

from datetime import date

__appname__    = "Pac-u-lator"
__author__     = "Kevin Cole"
__copyright__  = "Copyright \N{copyright sign} 2017"
__agency__     = "NOVA Web Development, LLC"
__credits__    = ["Kevin Cole"]  # Authors and bug reporters
__license__    = "GPL"
__version__    = "2.2"
__maintainer__ = "Kevin Cole"
__email__      = "kevin.cole@novawebcoop.org"
__status__     = "Prototype"  # "Prototype", "Development" or "Production"
__module__     = "configure"


def configure():
    """Configure Pac-u-Lator"""

    print("Configuring Pac-u-Lator...\n")

    source    = None
    broadcast = None
    ofile     = None
    force     = None
    ifile     = None

    earth_date = date.today()
    star_date  = f"{earth_date.year:04d}."
    star_date += f"{earth_date.month:02d}."
    star_date += f"{earth_date.day:02d}"

    with open("pacalib/configuration.fmt", "r") as fin:
        template = fin.read()

    config_path = os.path.expanduser("~/.config")
    config_file = f"{config_path}/paculator.conf"
    config = configparser.ConfigParser()
    config.read(config_file)

    default = config["devices"]["source"]
    prompt = f"Serial number for Paca dump source (0 - 4095) [{default}]: "
    while not isinstance(source, int) or source not in range(4096):
        source = input(prompt)
        if source == "":
            source = default
        try:
            source = int(source)
        except ValueError:
            continue
    config["devices"]["source"] = "{0}".format(source)

    default = config["devices"]["broadcast"]
    prompt = f"Serial number for Pac-u-lator      (0 - 4095) [{default}]: "
    while not isinstance(broadcast, int) or broadcast not in range(4096):
        broadcast  = input(prompt)
        if broadcast == "":
            broadcast = default
        try:
            broadcast = int(broadcast)
        except ValueError:
            continue
    config["devices"]["broadcast"] = "{0}".format(broadcast)

    default = config["files"]["out"][1:-1]
    prompt = f"Output JSON filename for Paca dumps (*.json) [{default}]: "
    while not isinstance(ofile, str) or not ofile.endswith(".json"):
        ofile  = input(prompt).lower()
        if ofile == "":
            ofile = default
    config["files"]["out"] = "\"{0}\"".format(ofile)

    default = config["files"]["force"]
    prompt = f"Overwrite existing dump file?   ('Y' or 'N') [{default}]: "
    while not isinstance(force, str) or force not in ("True", "False"):
        forcing = "Y" if default == "True" else "N"
        force  = input(prompt).upper()
        if force == "":
            force = default
        else:
            force = "True" if force == "Y" else "False"
    config["files"]["force"] = force

    default = config["files"]["in"][1:-1]
    prompt = f"Input JSON filename for Pac-u-lator loads (*.json) [{default}]: "
    while not isinstance(ifile, str) or not ifile.endswith(".json"):
        ifile  = input(prompt).lower()
        if ifile == "":
            ifile = default
    config["files"]["in"] = f'"{ifile}"'

    with open(config_file, "w") as new_config:
        new_config.write(template.format(star_date=star_date,
                                         source=source,
                                         broadcast=broadcast,
                                         ofile=ofile,
                                         force=force,
                                         ifile=ifile))

    print("")


def main():
    """Tast the configure() function"""

    _ = os.system("clear")
    print(f"{__appname__} v.{__version__}")
    print(f"{__copyright__} ({__license__})")
    print(f"{__author__}, {__agency__} <{__email__}>")
    print()

    configure()

    return 0


if __name__ == "__main__":
    main()
