#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  kwidgets.py
#
#  This will build an SVG from a Paca-dumpped JSON file in preparation for
#  mimicing the Kyma Virtual Control Surface (KWidgets)
#
#  Copyright 2017 Kevin Cole <kevin.cole@novawebcoop.org> 2017.12.23
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation; either version 3 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the Free
#  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, USA.
#
#  ACKNOWLEDGEMENTS
#
#  Many thanks to @eyllanesc who took a LOT of time with me:
#
#  * https://stackoverflow.com/users/6622587/eyllanesc
#  * https://gist.github.com/eyllanesc/01ef7eb60efc2554bb7bf69ef271b25c
#  * https://stackoverflow.com/questions/47972967/\
#    how-do-i-add-fit-and-position-dynamically-generated-qgroupbox-on-a-screen
#

"""Construct SVG version of Kyma widgets from JSON dump data"""

from PyQt5.QtCore    import *
from PyQt5.QtGui     import *
from PyQt5.QtWidgets import *

# Font
#
FONT = QFont()
FONT.setWeight(75)
FONT.setBold(True)

# Colors
#
KWIDGETS_OUTER_BG     = "#c0cbd9"  # "Link water"   (steel blue gray)
KWIDGETS_INNER_BG     = "#ccd8e7"  # "Hawkes blue"
KWIDGETS_UNFOCUSED    = "#d5d5d5"  # "Light grey"
KWIDGETS_GREEN_PUSHED = "#12f785"  # "Spring green"
KWIDGETS_GREEN_PULLED = "#9cd2b7"  # "Padua"        (hospital puke green)
KWIDGETS_RED_PUSHED   = "#de0d00"  # "Orange red"
KWIDGETS_RED_PULLED   = "#ce9a9a"  # "Rosy brown"   (pink-ish)


def rgb(pallet):
    """Scale the colors and convert to an RGB hexadecimal string"""

    hex_rgb  = f"""#"""
    hex_rgb += f"""{int(pallet["red"]   * 255):02x}"""
    hex_rgb += f"""{int(pallet["green"] * 255):02x}"""
    hex_rgb += f"""{int(pallet["blue"]  * 255):02x}"""
    return hex_rgb


def disemvowel(phrase):
    """Shorten phrases by removing vowels"""

    vowels = ["a", "e", "i", "o", "u"]
    words = phrase.split(" ")
    phrs = ""
    for word in words:
        if phrs:
            phrs += " "
        wrd = word[0]
        for letter in word[1:]:
            if letter not in vowels:
                wrd += letter
        phrs += wrd
    return phrs


class KWidgets(QWidget):
    """Generic Kyma widget"""

    def __init__(self, *args, **kwargs):
        """Construct a Dialog window and fill with widgets"""

        super(KWidgets, self).__init__(**kwargs)
        self.setObjectName("KWidgets")
        self.screen_width = None
        self.screen_height = None

        # Known widget display types. More someday.
        #
        display_types  = ["Fader",
                          "Select from List",
                          "Toggle"]

    def make_widget(self, specs, widget):
        """Makes a widget box and positions it"""

        box = QGroupBox(self)
        layout = specs["layout"]
        rect = QRect()
        rect.setTop(layout["topOffset"]       + 2)
        rect.setLeft(layout["leftOffset"]     + 2)
        rect.setBottom(layout["bottomOffset"] - 2)
        rect.setRight(layout["rightOffset"]   - 2)
        box.setGeometry(self.getRect(rect))

        box_css  = "QGroupBox {"
        box_css += "  border: 0px;"
        box_css += "  background-color: #ccd8e7;"
        box_css += "}"
        box.setStyleSheet(box_css)

    def getRect(self, rect):
        """Transforms Kyma coordinates to sceen coordinates"""

        screen_resolution = qApp.desktop().screenGeometry()
        self.screen_width  = screen_resolution.width()  - ((self.boundary * 2) + 12)
        self.screen_height = screen_resolution.height() - ((self.boundary * 2) + 12)

        transform = QTransform()
        transform.translate((self.boundary + 6),
                            (self.boundary + 6))
        transform.scale(self.screen_width / self.sizeKymaScreen.width(),
                        self.screen_height / self.sizeKymaScreen.height())
        return transform.mapRect(rect)


class KFader:
    """Kyma fader"""

    def __init__(self, specs):
        label = QLabel(box)
        label.setFont(FONT)
        label.setText(specs["label"])
        maxw = layout["rightOffset"] - layout["leftOffset"] - 4
        maxw *= (self.screen_width / self.sizeKymaScreen.width())
        labelw = label.fontMetrics().boundingRect(specs["label"]).width() + 12
        if labelw > maxw:
            label.setText(disemvowel(specs["label"]))
        label_height = label.height()
        spin_box = QSpinBox(box)  # See my Jukebox.py to finish
        spin_box.setRange(specs["minimum"], specs["maximum"])
        spin_box_height = spin_box.height()
        oldx, oldy = spin_box.pos().x(), spin_box.pos().y()
        newpos = QPoint(oldx, oldy + label_height)
        spin_box.move(newpos)
        control = QSlider(box)
        object_id = specs["label"].replace(" ", "")
        control.setObjectName(object_id)
        control.setToolTip(specs["label"])
        tallness = (layout["bottomOffset"] - layout["topOffset"] - 4)
        tallness *= (self.screen_height / self.sizeKymaScreen.height())
        tallness -= label_height
        tallness -= spin_box_height
        oldx, oldy = control.pos().x(), control.pos().y()
        newpos = QPoint(oldx, oldy + label_height + spin_box_height)
        control.move(newpos)
        control_css  = f"QSlider {{"
        control_css += f"  border: 0px;"
        control_css += f"  min-height: {tallness}px"
        control_css += f"}}"
        control_css += f"QSlider::handle {{"
        control_css += f"  background-color: #fe0f00;"
        control_css += f"  border: 1px solid #000000;"
        control_css += f"}}"

        control.setStyleSheet(control_css)
        control.setMinimum(specs["minimum"])
        control.setMaximum(specs["maximum"])
        control.setTickInterval(specs["grid"])
        control.setSingleStep(specs["grid"])
        control.setPageStep(specs["grid"])
        control.setTickPosition(QSlider.TicksAbove)


class KToggle(QPushButton):
    """Kyma toggle button"""

    def __init__(self, specs):
        control = QPushButton(box)
        if "tickMarksOrNil"  in specs and \
           isinstance(specs["tickMarksOrNil"], dict) and \
           "rawLabelsString" in specs["tickMarksOrNil"]:
            labels = specs["tickMarksOrNil"]["rawLabelsString"]
        else:
            labels = [specs["label"]]
        object_id = specs["label"].replace(" ", "")
        control.setObjectName(object_id)
        control.setText(labels[0])
        control.setToolTip(labels[0])
        try:
            colors   = specs["lookOrNil"]["database"]["flatAssociations"]
            off_color = rgb(colors["vcsButtonDownColor"])
            on_color  = rgb(colors["vcsButtonUpColor"])
        except KeyError:
            off_color = KWIDGETS_RED_PULLED
            on_color  = KWIDGETS_GREEN_PULLED
        control.setAutoDefault(False)
        control.setDefault(False)
        control.setCheckable(True)
        control.setChecked(False)
        control_css  = f"QPushButton#{object_id}:closed {{"
        control_css += f"  background-color: {off_color};"
        control_css += f"  border: 1px outset #000000;"
        control_css += f"  border-radius: 6px;"
        control_css += f"  padding: 6px;"
        control_css += f"}}"
        control_css += f"QPushButton#{object_id}:open {{"
        control_css += f"  background-color: {on_color};"
        control_css += f"  border: 1px inset #000000;"
        control_css += f"  border-radius: 6px;"
        control_css += f"  padding: 6px;"
        control_css += f"}}"

        control.setStyleSheet(control_css)
        maxw = layout["rightOffset"] - layout["leftOffset"] - 4
        maxw *= (self.screen_width / self.sizeKymaScreen.width())
        control.setMaximumWidth(maxw)
        control.setMinimumWidth(maxw)
        labelw = 0
        labelw = control.fontMetrics().boundingRect(labels[0]).width() + 12
        if labelw > maxw:
            control.setText(disemvowel(labels[0]))


class KList:
    """Kyma select from list pulldown menu"""

    def __init__(self, specs):
        label = QLabel(box)
        label.setFont(FONT)
        label.setText(specs["label"])
        maxw = layout["rightOffset"] - layout["leftOffset"] - 4
        maxw *= (self.screen_width / self.sizeKymaScreen.width())
        labelw = label.fontMetrics().boundingRect(specs["label"]).width() + 12
        if labelw > maxw:
            label.setText(disemvowel(specs["label"]))
        label_height = label.height()
        control = QComboBox(box)
        object_id = specs["label"].replace(" ", "")
        control.setObjectName(object_id)
        control.setToolTip(specs["label"])
        oldx, oldy = control.pos().x(), control.pos().y()
        newpos = QPoint(oldx, oldy + label_height)
        control.move(newpos)
        control_css  = "QComboBox {"
        control_css += "  border: 0px;"
        control_css += "}"
        control.setStyleSheet(control_css)
        choices = specs["tickMarksOrNil"]["rawLabelsString"]
        for index, choice in enumerate(choices):
            control.addItem(choice)


class KDial:
    """Kyma dial / knob"""

    def __init__(self, specs):
        label = QLabel(box)
        label.setFont(FONT)
        label.setText(specs["label"])
        maxw = layout["rightOffset"] - layout["leftOffset"] - 4
        maxw *= (self.screen_width / self.sizeKymaScreen.width())
        labelw = label.fontMetrics().boundingRect(specs["label"]).width() + 12
        if labelw > maxw:
            label.setText(disemvowel(specs["label"]))
        label_height = label.height()
        control = QDial(box)
        tallness  = (layout["bottomOffset"] - layout["topOffset"] - 4)
        tallness *= (self.screen_height / self.sizeKymaScreen.height())
        tallness -= label_height
        oldx, oldy = control.pos().x(), control.pos().y()
        control.setGeometry(oldx, oldy, tallness, tallness)
        object_id = specs["label"].replace(" ", "")
        control.setObjectName(object_id)
        control.setToolTip(specs["label"])
        if "tickMarksOrNil"  in specs and \
           isinstance(specs["tickMarksOrNil"], dict) and \
           "rawLabelsString" in specs["tickMarksOrNil"]:
            labels = specs["tickMarksOrNil"]["rawLabelsString"]
        else:
            labels = [specs["label"]]
        object_id = specs["label"].replace(" ", "")
        control.setObjectName(object_id)
        control.setToolTip(labels[0])
        oldx, oldy = control.pos().x(), control.pos().y()
        newpos = QPoint(oldx, oldy + label_height)
        control.move(newpos)
        maxw = layout["rightOffset"] - layout["leftOffset"] - 4
        maxw *= (self.screen_width / self.sizeKymaScreen.width())
        control.setMaximumWidth(maxw)
        control.setMinimumWidth(maxw)


class KCheck:
    """Kyma checkbox"""

    def __init__(self, specs):
        control = QCheckBox(box)
        object_id = specs["label"].replace(" ", "")
        control.setObjectName(object_id)
        control.setToolTip(specs["label"])
        if "tickMarksOrNil"  in specs and \
           isinstance(specs["tickMarksOrNil"], dict) and \
           "rawLabelsString" in specs["tickMarksOrNil"]:
            labels = specs["tickMarksOrNil"]["rawLabelsString"]
        else:
            labels = [specs["label"]]
        object_id = specs["label"].replace(" ", "")
        control.setObjectName(object_id)
        control.setText(labels[0])
        control.setToolTip(labels[0])
        try:
            colors   = specs["lookOrNil"]["database"]["flatAssociations"]
            off_color = rgb(colors["vcsButtonDownColor"])
            on_color  = rgb(colors["vcsButtonUpColor"])
        except KeyError:
            off_color = KWIDGETS_RED_PULLED
            on_color  = KWIDGETS_GREEN_PULLED
        control_css  = f"QCheckBox#{object_id}:closed {{"
        control_css += f"  background-color: {off_color};"
        control_css += f"  border: 1px outset #000000;"
        control_css += f"  border-radius: 6px;"
        control_css += f"  padding: 6px;"
        control_css += f"}}"
        control_css += f"QCheckBox#{object_id}:open {{"
        control_css += f"  background-color: {on_color};"
        control_css += f"  border: 1px inset #000000;"
        control_css += f"  border-radius: 6px;"
        control_css += f"  padding: 6px;"
        control_css += f"}}"

        control.setStyleSheet(control_css)
        maxw = layout["rightOffset"] - layout["leftOffset"] - 4
        maxw *= (self.screen_width / self.sizeKymaScreen.width())
        control.setMaximumWidth(maxw)
        control.setMinimumWidth(maxw)
        labelw = 0
        labelw = control.fontMetrics().boundingRect(labels[0]).width() + 12
        if labelw > maxw:
            control.setText(disemvowel(labels[0]))
