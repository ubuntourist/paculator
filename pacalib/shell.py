#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  shell.py
#
#  Copyright 2017 Kevin Cole <kevin.cole@novawebcoop.org> 2017.12.22
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation; either version 3 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the Free
#  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, USA.
#
#

"""Handle shell command"""

import os
import sys
import cmd                             # command interpreter w/ tab completion
import readline                        # Command line history
import atexit                          # Interpreter exit handlers

from .lister import listpaca
from .config import configure
from .start  import start
from .vcs    import vcs

__appname__    = "Pac-u-lator"
__module__     = "shell"
__author__     = "Kevin Cole"
__copyright__  = "Copyright \N{copyright sign} 2017"
__agency__     = "NOVA Web Development, LLC"
__credits__    = ["Kevin Cole"]  # Authors and bug reporters
__license__    = "GPL"
__version__    = "2.2"
__maintainer__ = "Kevin Cole"
__email__      = "kevin.cole@novawebcoop.org"
__status__     = "Prototype"  # "Prototype", "Development" or "Production"


class Shell(cmd.Cmd):
    """Interactive Pac-u-Lator shell"""

    intro = "Type help or ? for a list of commands.\n"
    prompt = "> "

    # Set up persistent command line history
    #
    histdir  = os.path.expanduser("~/.local/share/paca/")
    histfile = os.path.join(histdir, "history")
    if not os.path.isfile(histfile):      # If there's no history file...
        empty = open(histfile, "a")       # ... create an ALMOST empty one...
        empty.write("_HiStOrY_V2_\n")     # ... with the special header line
        empty.close()
    readline.read_history_file(histfile)  # Read history from old sessions
    readline.set_history_length(1000)     # Default length was -1 (infinite)

    atexit.register(readline.write_history_file,
                    histfile)             # Save history on exit

    def __init__(self, config):
        """Initialize the interactive shell"""
        super(Shell, self).__init__()
        self.config = config

    def emptyline(self):
        """Ignore empty command lines"""
        pass

    def do_list(self, arg):
        """List addresses of all Paca(rana) and Pac-u-lators on the network"""
        listpaca()

    def do_configure(self, arg):
        """Set defaults: source & broadcast devices, dump & load filenames"""
        configure()

    def do_dump(self, arg):
        """Dump Paca(rana) or Pac-u-lator memory to JSON file"""
        pass

    def do_start(self, arg):
        """Start listening and serving OSC requests with console display"""
        if arg != "":
            args = arg.split(" ")
            if len(args) == 1:
                args.append(self.config["devices"]["broadcast"])
        else:
            args = [self.config["files"]["in"][1:-1],
                    self.config["devices"]["broadcast"]]
        zc, st = start(args[0], args[1])

    def do_vcs(self, arg):
        """Start listening and serving OSC requests with VCS display"""
        if arg != "":
            args = arg.split(" ")
            if len(args) == 1:
                args.append(self.config["devices"]["broadcast"])
        else:
            args = [self.config["files"]["in"][1:-1],
                    self.config["devices"]["broadcast"]]
        vcs(args[0], args[1])

    def do_debug(self, arg):
        """Debug the Pac-u-lator"""
        pass

    def do_quit(self, arg):
        """Exit Pac-u-lator"""
        sys.exit(0)

#   def do_help(self, arg):
#       """xxx"""
#       pass


def main():
    """The main attraction (for stand-alone testing)"""

    _ = os.system("clear")
    print("{__appname__} v.{__version__}")
    print(f"{__copyright__} ({__license__})")
    print(f"{__author__}, {__agency__} <{__email__}>")
    print()

    Shell().cmdloop()

    return 0


if __name__ == "__main__":
    main()
