#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#  Copyright 2016 Kevin Cole <kevin.cole@novawebcoop.org> 2016.05.13
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation; either version 3 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the Free
#  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, USA.
#

"""Symbolic Sound Paca Emulator"""

from itertools  import zip_longest

import sys
import os.path                    # File and directory manipulations

import json                       # For JSON feedback messages from Paca
import threading                  # Could it be this simple???

import socket                     # Avahi "dependency"
from   time      import sleep     # Slow down the requests to Paca
from   zeroconf  import *         # Avahi (a.k.a. Bonjour, Zeroconf, etc.)

from pythonosc   import udp_client           # Open Sound Control UDP
from pythonosc   import osc_message_builder  # Build messages sent to Paca
from pythonosc   import dispatcher           # Handle messages from Paca
from pythonosc   import osc_server           # Listen for messages from Paca

import netifaces as ni           # We are the network interfaces that say "Ni!"

__author__     = "Kevin Cole"
__copyright__  = "Copyright 2016, Kevin Cole (2016.05.13)"
__credits__    = ["Kevin Cole"]  # Authors and bug reporters
__license__    = "GPL"
__version__    = "2.2"
__maintainer__ = "Kevin Cole"
__email__      = "kevin.cole@novawebcoop.org"
__status__     = "Development"  # "Prototype", "Development" or "Production"
__appname__    = "Paca OSC Simulator"

# Unicode silliness to avoid NameError exeptions
#
if   sys.version_info.major == 2: stringTypes = (basestring,)
elif sys.version_info.major == 3: stringTypes = (str,)

ESC = chr(27)
SECONDS = 0.05  # 0.01 is not enough sleep time for Paca, but 0.05 works.

jsons = []
soundscapes = []


# See https://docs.python.org/3/library/itertools.html#itertools-recipes
#
def grouper(iterable, fillvalue=None):
    "Collect data into fixed-length chunks or blocks"
    # grouper('ABCDEFG', 3, 'x') --> ABC DEF Gxx"
    args = [iter(iterable)] * 2
    return zip_longest(*args, fillvalue=fillvalue)


class Paca(object):
    """Symbolic Sound Paca Emulator"""

    def __init__(self, widgets):
        """Set up response handlers and broadcast existence"""

        try:
            nic = ni.gateways()["default"][ni.AF_INET][1]
            broadcast = ni.ifaddresses(nic)[ni.AF_INET][0]["broadcast"]
        except KeyError:
            print("No network found. Falling back to localhost.")
            broadcast = "127.0.0.1"
        except OSError:
            print("Stupid operating system found. Falling back to localhost.")
            broadcast = "127.0.0.1"
        self.osc = udp_client.UDPClient(broadcast, 8001, allow_broadcast=True)

        self.response_port = None
        self.notification  = False
        self.widgets = widgets

        # Set up the server (listener)
        self.director = dispatcher.Dispatcher()
        self.director.map("/osc/respond_to", self.handle_respond_to)
        self.director.map("/osc/notify/vcs/SoundAdvice", self.count_widgets)
        self.director.map("/osc/widget", self.handle_widget)
        self.director.map("/vcs", self.handle_vcs)
        self.director.set_default_handler(self.wtf)

#       self.server = osc_server.ThreadingOSCUDPServer(("0.0.0.0", 8000),
#                                                      self.director)
        self.server = osc_server.ForkingOSCUDPServer(("0.0.0.0", 8000),
                                                     self.director)

        self.server_thread = threading.Thread(target=self.server.serve_forever)

    def get_by_id(self, event_id):
        """Retrieve a widget based on its event ID"""
        for widget in self.widgets:
            if "concreteEventID" in widget:
                if widget["concreteEventID"] == event_id:
                    return widget

    def handle_respond_to(self, addr, args):
        """Handles those rare events where Paca sends back an integer"""
        print(f"DEBUG: handle_response_to responding to {addr}, {args}")
        osc_response_from = "/osc/response_from"
        msg = osc_message_builder.OscMessageBuilder(address=osc_response_from)
        msg.add_arg(34789)
        msg = msg.build()
        self.osc.send(msg)
        sleep(SECONDS)
        print(f"{ESC}[31;1m{addr} {args}{ESC}[0m")

    def handle_widget(self, addr, args):
        """Return JSON representation of widget"""
        print(f"DEBUG: handle_widget responding to {addr}, {args}")

        try:
            widget = json.dumps(self.widgets[args])
        except IndexError:
            widget = ""

        print(f"{ESC}[35;1maddr: {addr}{ESC}[0m")
        print(f"{ESC}[35;1margs: {args}{ESC}[0m")
        print(f"{ESC}[35;1mnary: {widget}{ESC}[0m")

        msg = osc_message_builder.OscMessageBuilder(address=addr)
        msg.add_arg(args)
        msg.add_arg(widget)
        msg = msg.build()
        self.osc.send(msg)
        sleep(SECONDS)

    def count_widgets(self, addr, args):
        """Respond with number of widgets available"""

        print(f"DEBUG: count_widgets responding to {addr}, {args}")
        msg = osc_message_builder.OscMessageBuilder(address=addr)
        msg.add_arg(len(self.widgets))
        msg = msg.build()
        self.osc.send(msg)
        sleep(SECONDS)

    def handle_vcs(self, addr, *args):
        """Respond to /vcs messages"""

        print(f"DEBUG: handle_vcs responding to {addr}, {args}")
        data = f"{ESC}[31;1m{addr}"
        for i in range(len(args)):
            data += f" {{{i + 2}}}"
        data += f"{ESC}[0m"
        print(data)
#       msg = osc_message_builder.OscMessageBuilder(address=addr)
        for addr, arg in grouper(args):
            print(f"    {addr}  {arg}")
        for addr, arg in grouper(args):
            widget = self.get_by_id(addr)
            command = f"""/vcs/{widget["label"]}/1"""
            msg = osc_message_builder.OscMessageBuilder(address=command)
            msg.add_arg(arg)
            msg = msg.build()
            self.osc.send(msg)
            sleep(SECONDS)

    def wtf(self, *args, **kwargs):
        """WTF???"""
        print("DEBUG: wtf responding")
        if args is not None:
            for x, arg in enumerate(args):
                print(f"arg {x}: {arg}")
        else:
            print("No regular args")
        if kwargs is not None:
            for key, value in kwargs.items():
                print(f"arg {key}: {value}")
        else:
            print("No keyword args")


def main():
    """Compile, load and start the "Sound" in Paca"""

    wd = os.path.expanduser("~/.local/share/paca")
    kyma = f"{wd}/kyma.json"

    dump = open(kyma, "r")
    widgets = json.load(dump)

    paca = Paca(widgets)

    paca.server_thread.start()


if __name__ == "__main__":
    address = None
    try:
        address = socket.gethostbyname(f"{socket.gethostname()}.local")
    except socket.error:
        print("No network found. Falling back to localhost.")
        address = socket.gethostbyname("localhost")

    props = {}
    # Was "beslime-833._osc._udp.local."
    info = ServiceInfo("_osc._udp.local.",
                       "beslime-0._osc._udp.local.",
                       addresses=[socket.inet_aton(address)], port=8000,
                       weight=0, priority=0,
                       properties=props,
                       server="beslime-0.local.")
    zeroconf = Zeroconf()
    zeroconf.register_service(info, ttl=36000)

    main()
