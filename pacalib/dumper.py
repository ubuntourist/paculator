#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  Dump Paca
#
#  Copyright 2017 Kevin Cole <kevin.cole@novawebcoop.org> 2017.03.07
#
#  Saves Paca's guts to a JSON file for the emulator
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation; either version 3 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the Free
#  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, USA.
#

"""Handle dump command"""

import sys
import os
from   time import sleep

import json       # For JSON feedback messages from Paca
import struct     # For floating point and integer feedback messages from Paca
import threading  # For asynchronous listening for Paca responses

from pythonosc import udp_client           # Open Sound Control UDP
from pythonosc import osc_message_builder  # Build messages sent to Paca
from pythonosc import dispatcher           # Handle messages returned from Paca
from pythonosc import osc_server           # Listen for messages from Paca

# Unicode silliness to avoid NameError exeptions (Python 2 vs. Python 3)
#
if   sys.version_info.major == 2: stringTypes = (basestring,)
elif sys.version_info.major == 3: stringTypes = (str,)

SECONDS = 0.05  # 0.01 is not enough sleep time for Paca, but 0.05 works.


def listify(obj):
    """Recurse thru dictionary splitting multi-line string values into lists"""
    for field in obj:
        if isinstance(obj[field], dict):
            listify(obj[field])
        elif isinstance(obj[field], stringTypes):
            if obj[field][-1] == u"\r":       # If string ends with newline...
                obj[field] = obj[field][:-1]  # ...remove it
            obj[field] = obj[field].split(u"\r")  # Paca uses Mac newline
            if len(obj[field]) == 1:        # If "list" is only one element...
                obj[field] = obj[field][0]  # ...unlist it
    return obj


class Dumper:
    """OSC driver for Paca(rana) memory to JSON dumper"""

    def __init__(self, out="kyma.json", source="0"):
        """Connect with a Paca(rana)"""

        fqdn = f"beslime-{source}.local"
#       self.out = input("Save as... [*.json]: ")
        self.out = out
        self.source = udp_client.UDPClient(fqdn, 8000, allow_broadcast=False)
        self.naries = {}

        # Dummy send to trigger socket.gaierror when no device present
        #
#       oscRespondTo = "/osc/respond_to"
#       msg = osc_message_builder.OscMessageBuilder(address=oscRespondTo)
#       msg.add_arg(8001)  # Send feedback to port 8001
#       msg = msg.build()
#       self.source.send(msg)
#       sleep(SECONDS)

        # Set up the server (listener)
        director = dispatcher.Dispatcher()
        director.map("/osc/widget", self.handle_json)
        director.map("/osc/notify/vcs/SoundAdvice", self.fetch_widgets)
        director.map("/vcs", self.handle_float)  # Handles all our directives

        self.server = osc_server.ThreadingOSCUDPServer(("0.0.0.0", 8001),
                                                       director)
#       self.server = osc_server.ForkingOSCUDPServer(("0.0.0.0", 8001),
#                                                    director)
        self.server_thread = threading.Thread(target=self.server.serve_forever)

        self.server_thread.start()

        oscRespondTo = "/osc/respond_to"
        msg = osc_message_builder.OscMessageBuilder(address=oscRespondTo)
        msg.add_arg(8001)  # Send feedback to port 8001
        msg = msg.build()
        self.source.send(msg)
        sleep(SECONDS)

        oscNotify = "/osc/notify/vcs/SoundAdvice"
        msg = osc_message_builder.OscMessageBuilder(address=oscNotify)
        msg.add_arg(1)  # Turn notifications ON (0 for OFF)
        msg = msg.build()
        self.source.send(msg)
        sleep(SECONDS)

        wd = os.path.expanduser("~/.local/share/paca")
        if not os.path.exists(wd):
            os.makedirs(wd, 0o755)
        out = os.path.splitext(out)[0]    # Eliminate the extension if provided
        full_path = f"{wd}/{out}.json"
        self.out = open(full_path, "w")

        print(f"Paca(rana) ID: beslime-{source}.local")
        print(f"Kyma Sound:    {out}")

    def handle_json(self, address, args, stream):
        """Objectifies the JSON string returned by '/osc/widget #'."""
        nary = listify(json.loads(stream))  # Always a dictionary, we hope.
        if args not in self.naries:
            self.naries[args] = nary
        sleep(SECONDS)

    def fetch_widgets(self, address, args):
        """Request the info on all widgets"""
        while len(self.naries) != args:
            for widget in range(args):
                oscWidget = "/osc/widget"
                msg = osc_message_builder.OscMessageBuilder(address=oscWidget)
                msg.add_arg(widget)  # Send feedback to port 8000
                msg = msg.build()
                self.source.send(msg)
                sleep(SECONDS)
        kees = list(self.naries.keys())
        kees.sort()
        final = []
        for kee in kees:
            final.append(self.naries[kee])
        output = json.dumps(final, sort_keys=True,
                            indent=4, separators=(',', ': '))
        self.out.write(output)
        self.out.close()
        self.server.shutdown()

    def handle_float(self, address, args):
        """Handle messages that return values between 0.0 and 1.0"""
        blobs = [args[start:start + 8] for start in range(0, len(args), 8)]
        pairs = []
        for blob in blobs:
            widgetId =       struct.unpack("!i", blob[0:4])[0]
            value    = round(struct.unpack("!f", blob[4:8])[0], 3)
            pairs.append((widgetId, value))


def main():
    """The main attraction (for stand-alone testing)"""

    device = sys.argv[1] if len(sys.argv) > 1 else "0"
    dumper = Dumper(out="debug.json", source=device)


if __name__ == "__main__":
    main()
