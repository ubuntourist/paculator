#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  list.py
#
#  List available Pacas / Pac-u-lators
#
#  Copyright 2017 Kevin Cole <kevin.cole@novawebcoop.org> 2017.11.13
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation; either version 3 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the Free
#  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, USA.
#

"""Handle list command"""

from .finder import find_pacas

__appname__    = "List Pacas / Pac-u-lators"
__author__     = "Kevin Cole"
__copyright__  = "Copyright \N{copyright sign} 2017"
__agency__     = "NOVA Web Development, LLC"
__credits__    = ["Kevin Cole"]  # Authors and bug reporters
__license__    = "GPL"
__version__    = "2.2"
__maintainer__ = "Kevin Cole"
__email__      = "kevin.cole@novawebcoop.org"
__status__     = "Prototype"  # "Prototype", "Development" or "Production"
__module__     = ""


def listpaca():
    """List all network-reachable Paca(rana)s and Pac-u-Lators"""

    print("[Pinging the network for Pacas... Wait...]\n")
    pacas = find_pacas()
    if not pacas:
        print("No active Paca(rana)s or Pac-u-lators found.")
    elif len(pacas) == 1:
        print("Found 1 active Paca(rana) or Pac-u-lator:\n")
        for paca in pacas:
            print(f"    {paca[0]} ({paca[1]})")
    else:
        print(f"Found {len(pacas)} active Paca(rana)s and/or Pac-u-lators:")
        print()
        for count, paca in enumerate(pacas):
            print(f"    {count + 1}. {paca[0]} ({paca[1]})")
    print("")


def main():
    """The main attraction (for stand-alone testing)"""
    listpaca()


if __name__ == "__main__":
    main()
