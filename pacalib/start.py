#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Paca OSC Simulator

This sends semi-realistic Paca OSC responses to OSC requests, so
that the main routines will no longer be dependent on an actual Paca
for testing.

It now  advertises itself as a Rendezvous / Bonjour / ZeroConf / Avahi
service!

See: Creating a program to be broadcasted by Avahi

  http://stackoverflow.com/questions/1534655/\
  creating-a-program-to-be-broadcasted-by-avahi/18104922
"""

#  See https://docs.python.org/3.3/tutorial/classes.html
#
#  Copyright 2016 Kevin Cole <kevin.cole@novawebcoop.org> 2016.05.13
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation; either version 3 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the Free
#  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, USA.
#
#

import sys
import os.path    # File and directory manipulations

import json       # For JSON feedback messages from Paca

import socket                    # Avahi "dependency"
from   zeroconf import *         # Avahi (a.k.a. Bonjour, Zeroconf, etc.)

from .Paca import Paca

from .finder import find_pacas

__appname__    = "Pac-u-lator"
__author__     = "Kevin Cole"
__copyright__  = "Copyright \N{copyright sign} 2018"
__credits__    = ["Kevin Cole"]  # Authors and bug reporters
__license__    = "GPL"
__version__    = "2.2"
__maintainer__ = "Kevin Cole"
__email__      = "kevin.cole@novawebcoop.org"
__status__     = "Development"  # "Prototype", "Development" or "Production"
__module__     = "start"

# Unicode silliness to avoid NameError exeptions
#
if   sys.version_info.major == 2: stringTypes = (basestring,)
elif sys.version_info.major == 3: stringTypes = (str,)


def start(dump="kyma.json", broadcast="4095"):
    """Start a Pac-u-lator CLI"""

    pacas = find_pacas()
    for paca in pacas:
        if paca[0].find(broadcast) > -1:
            print(f"ERROR: beslime-{broadcast}.local already in use")
            sys.exit(1)

    wd = os.path.expanduser("~/.local/share/paca")
    dump = os.path.splitext(dump)[0]    # Eliminate extension
    full_path = f"{wd}/{dump}.json"

    sound = open(full_path, "r")
    widgets = json.load(sound)

    paca = Paca(widgets)
    paca.server_thread.start()

    try:
        addr = socket.gethostbyname(f"{socket.gethostname()}.local")
    except socket.error:
        print("No network found. Falling back to localhost.")
        addr = socket.gethostbyname("localhost")
    props = {}
    # Was "beslime-833._osc._udp.local."
    beslime = f"beslime-{broadcast}"
    info = ServiceInfo("_osc._udp.local.",
                       f"{beslime}._osc._udp.local.",
                       addresses=[socket.inet_aton(addr)], port=8000,
                       weight=0, priority=0,
                       properties=props,
                       server=f"{beslime}.local.")
    zeroconf = Zeroconf()
    zeroconf.register_service(info, ttl=36000)

    print(f"Paca(rana) ID: beslime-{broadcast}.local")
    print(f"Kyma Sound:    {dump}")

    return zeroconf, paca.server_thread


def main():
    """Compile, load and start the "Sound" in Paca"""

    if len(sys.argv) == 2:
        kyma = sys.argv[1]
    elif len(sys.argv) == 1:
        kyma = input("Paca JSON dump file [kyma.json]: ")
    else:
        print("Too many arguments provided")
        sys.exit(2)  # 2 = Unix command line syntax error
    if kyma == "":
        kyma = "kyma"
    zc, st = start(dump=kyma)


if __name__ == "__main__":
    main()
