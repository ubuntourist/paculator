#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  list.py
#
#  List available Pacas / Pac-u-lators
#
#  Copyright 2017 Kevin Cole <kevin.cole@novawebcoop.org> 2017.11.13
#
#  Portions copied from:
#
#    How can I find the IP address of a host using mDNS?
#    https://stackoverflow.com/a/36257360/447830
#
#  A stripped down verssion of browser.py example.
#  (NOTE: zeroconf may have issues with IPV6 addresses
#         and mixed-case hostnames.)
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation; either version 3 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the Free
#  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, USA.
#

"""Find hostnames and IPv4 addresses of Paca(rana)s / Pac-u-lators"""

import os
from   time     import sleep
from   zeroconf import ServiceBrowser, Zeroconf
from   zeroconf import ServiceStateChange, DNSAddress

__appname__    = "List Pacas / Pac-u-lators"
__author__     = "Kevin Cole"
__copyright__  = "Copyright \N{copyright sign} 2017"
__agency__     = "NOVA Web Development, LLC"
__credits__    = ["Kevin Cole"]  # Authors and bug reporters
__license__    = "GPL"
__version__    = "2.2"
__maintainer__ = "Kevin Cole"
__email__      = "kevin.cole@novawebcoop.org"
__status__     = "Prototype"  # "Prototype", "Development" or "Production"
__module__     = ""


def on_service_state_change(zeroconf, service_type, name, state_change):
    """Handle devices coming online or going offline"""
    if state_change is ServiceStateChange.Added:
        zeroconf.get_service_info(service_type, name)


def find_pacas():
    """Return a list of Paca (hostname, IPv4) tuples"""
    pacas = []

    zeroconf = Zeroconf()
    ServiceBrowser(zeroconf, "_osc._udp.local.",
                   handlers=[on_service_state_change])
    sleep(2)

    cache = zeroconf.cache.cache
    zeroconf.close()

    for key in cache:
        if "beslime" in key and isinstance(cache[key][0], DNSAddress):
            ip = ""
            for octet in cache["beslime-4095.local."][0].address:
                ip += f"{octet}."
            pacas.append((key[:-1], ip[:-1]))
    pacas.sort(key=lambda tup: tup[0])
    return pacas


def main():
    """The main attraction (for stand-alone testing)"""

    _ = os.system("clear")
    print(f"{__appname__} v.{__version__}")
    print(f"{__copyright__} ({__license__})")
    print(f"{__author__}, {__agency__} <{__email__}>")

    print("[Pinging the network for Pacas... Wait...]\n")
    pacas = find_pacas()
    for paca in pacas:
        print(paca)


if __name__ == "__main__":
    main()
