#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  Pac-u-lator - a Paca OSC Emulator
#
#  Copyright 2016 Kevin Cole <kevin.cole@novawebcoop.org> 2016.05.13
#
#  This sends semi-realistic Paca OSC responses to OSC requests, freeing
#  applications from their dependency on an actual Paca during development
#  and testing.
#
#  It now advertises itself as a Rendezvous / Bonjour / ZeroConf / Avahi
#  service!
#
#  See: Creating a program to be broadcasted by Avahi
#
#    http://stackoverflow.com/questions/1534655/\
#    creating-a-program-to-be-broadcasted-by-avahi/18104922
#
#  See https://docs.python.org/3.3/tutorial/classes.html
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License as
#  published by the Free Software Foundation; either version 3 of
#  the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public
#  License along with this program; if not, write to the Free
#  Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#  Boston, MA 02110-1301, USA.
#

"""Usage:
    paculator list
    paculator configure
    paculator dump [-s <sn>] [-o <sound.json>]
    paculator start [-b <sn>] [-i <sound.json>]
    paculator vcs [-b <sn>] [-i <sound.json>]
    paculator (-h | --help | --version)
    paculator shell
    paculator debug

"""

import sys
import os.path       # File and directory manipulations
import socket        # Networking
import configparser  # Configuration parser
import signal        # Handle Ctrl-C interrupts

from docopt import docopt  # A better command line option / argument parser

from pacalib.lister import listpaca
from pacalib.dumper import Dumper
from pacalib.config import configure
from pacalib.shell  import Shell
from pacalib.start  import start
from pacalib.vcs    import vcs

__author__     = "Kevin Cole"
__copyright__  = "Copyright 2017, Kevin Cole (2017.11.10)"
__credits__    = ["Kevin Cole"]  # Authors and bug reporters
__license__    = "GPL"
__version__    = "2.2"
__maintainer__ = "Kevin Cole"
__email__      = "kevin.cole@novawebcoop.org"
__status__     = "Development"  # "Prototype", "Development" or "Production"
__appname__    = "Paca OSC Emulator"

# Unicode silliness to avoid NameError exeptions
#
if   sys.version_info.major == 2: stringTypes = basestring,
elif sys.version_info.major == 3: stringTypes = str,

ESC = chr(27)

jsons = []
soundscapes = []
passer = {}


def flush_network(signum, frame):
    """Clean-up in aisle 3, on exit"""

    passer["thread"].join(timeout=0.5)
    passer["zeroconf"].unregister_all_services()
    print("\nStopping server...")
    os._exit(0)


def validate_sn(options, option):
    """Validate supplied serial number"""

    error  = f"ERROR: {option} serial number must be "
    error += f"an integer between 0 and 4095"
    try:
        value = int(options[option])
    except ValueError:
        print(error)
        sys.exit(1)
    if value not in range(4096):
        print(error)
        sys.exit(1)


def main():
    """Compile, load and start the "Sound" in Paca"""
    signal.signal(signal.SIGINT, flush_network)

    _ = os.system("clear")
    print(f"{__appname__} version {__version__}")
    print(f"{__copyright__} [{__license__}]")

    wd = os.path.expanduser("~/.config")
    config_file = f"{wd}/paculator.conf"
    config = configparser.ConfigParser()
    config.read(config_file)

    options = f"""Options:
    --version               show version number and exit
    -h, --help              show this help message and exit
    -b, --broadcast=<sn>    broadcast serial number (0 - 4095)
                            [default: {config["devices"]["broadcast"]}]
    -i, --in=<sound.json>   Kyma Sound file (JSON dumped by dumpPaca)
                            [default: {config["files"]["in"][1:-1]}]
    -o, --out=<sound.json>  Kyma Sound file (JSON dumped by dumpPaca)
                            [default: {config["files"]["out"][1:-1]}]
    -s, --source=<sn>       dump from serial number (0 - 4095)
                            [default: {config["devices"]["source"]}]
    """

    parsed = docopt(__doc__ + options)

    if parsed["list"]:
        listpaca()
        sys.exit(0)

    elif parsed["dump"]:
        validate_sn(parsed, "--source")
        try:
            Dumper(out=parsed["--out"], source=parsed["--source"])
        except socket.gaierror:
            fqdn = f"""beslime-{parsed["--source"]}.local"""
            print(f"No Paca(rana) or Pac-u-lator running at {fqdn}")
            print("\nTry:\n")
            print("      'paculator list'       to find all available devices.")
            print("\nThen:\n")
            print("      'paculator configure'  to set a default")
            print("or    'paculator dump -s #'  to specify the serial number of the device\n")
            sys.exit(1)
        sys.exit(0)

    elif parsed["configure"]:
        configure()
        sys.exit(0)

    elif parsed["shell"]:
        Shell(config).cmdloop()

    elif parsed["start"]:
        validate_sn(parsed, "--broadcast")
        zc, st = start(dump=parsed["--in"], broadcast=parsed["--broadcast"])
        passer["zeroconf"] = zc
        passer["thread"]   = st
        sys.exit(0)

    elif parsed["vcs"]:
        validate_sn(parsed, "--broadcast")
        vcs(dump=parsed["--in"], broadcast=parsed["--broadcast"])
        sys.exit(0)

    elif parsed["debug"]:
        print(parsed)
        sys.exit(0)

    elif parsed["--version"]:
        sys.exit(0)

    else:
        print(parsed)
        sys.exit(1)


if __name__ == "__main__":
    main()
